"""Bexio target sink class, which handles writing streams."""
from datetime import datetime, timedelta
import backoff
import requests
from singer_sdk.exceptions import FatalAPIError, RetriableAPIError
from singer_sdk.sinks import RecordSink


class BexioSink(RecordSink):
    """Bexio target sink class."""

    base_url = "https://api.bexio.com"
    access_token = None
    expires_at = None

    def get_auth(self):

        if self.access_token is None or self.expires_at <= datetime.utcnow():
            response = requests.post(
                "https://idp.bexio.com/token",
                data={
                    "client_id": self.config.get("client_id"),
                    "client_secret": self.config.get("client_secret"),
                    "refresh_token": self.config.get("refresh_token"),
                    "grant_type": "refresh_token",
                },
            )

            if response.status_code != 200:
                raise Exception(response.text)

            data = response.json()

            self.access_token = data["access_token"]

            self.expires_at = datetime.utcnow() + timedelta(
                seconds=int(data["expires_in"]) - 10
            )  # pad by 10 seconds for clock drift

        return self.access_token

    def get_headers(self):
        headers = {}
        headers["Accept"] = "application/json"
        headers["Content-Type"] = "application/json"
        headers["Authorization"] = f"Bearer {self.get_auth()}"
        return headers

    def entity_lookup(
        self,
        search_value,
        search_stream="2.0/contact/search",
        field="name_1",
        criteria="=",
    ):
        payload = {"field": field, "value": search_value, "criteria": criteria}
        url = f"{self.base_url}/{search_stream}"
        return requests.request(
            "POST", url, json=[payload], headers=self.get_headers()
        ).json()

    def prepare_payload(self, record):
        payload = record
        payload.update({"positions": {"required": record["LineItems"]}})
        del payload["LineItems"]
        customer = self.entity_lookup(record["contact_name"])
        if len(customer) > 0:
            payload["contact_id"] = customer[0]["id"]
            del payload["contact_name"]
        else:
            # Skip record because contact is required
            return None
        return payload

    

    @backoff.on_exception(backoff.expo, (requests.exceptions.RequestException, requests.exceptions.ReadTimeout, RetriableAPIError), max_time=180)
    def make_request(self, method,url, headers,data):
        response = requests.request(method, url, json=data, headers=headers)
        self.validate_response(response)
        res = response.json()
        print(f"Record ID: {res['id']} successfully added")
        return response

    def validate_response(self, response: requests.Response) -> None:
        """Validate HTTP response."""
        if 400 <= response.status_code < 500:
            http_error_msg = (
                f"{response.status_code} Client Error: {response.reason} for url: {response.url}"
            )
            self.logger.error(http_error_msg)
            raise FatalAPIError(http_error_msg)
        elif 500 <= response.status_code < 600 :
            msg = (
                f"{response.status_code} Server Error: {response.reason} for url: {response.url}"
            )
            raise RetriableAPIError(msg)
   

    def process_record(self, record: dict, context: dict) -> None:
        """Process the record."""
        headers = self.get_headers()
        if self.stream_name == "createPO":
            if record:
                record = self.prepare_payload(record)
                url = f"{self.base_url}/3.0/purchase_orders"
                response = self.make_request("POST", url, headers, record)
               
        elif self.stream_name == "updatePO":
            record = self.prepare_payload(record)
            if record:
                url = f"{self.base_url}/3.0/purchase_orders/{record['id']}"
                response = self.make_request("PUT", url, headers, record)
               
        elif self.stream_name == "createPOraw":
            if record:
                if record.get('id') is not None:
                    url = f"{self.base_url}/3.0/purchase_orders/{record['id']}"
                    response = self.make_request("PUT", url, headers, record)
                   
                else:
                    url = f"{self.base_url}/3.0/purchase_orders"
                    response = self.make_request("POST", url, headers, record)
                    