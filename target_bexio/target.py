"""Bexio target class."""

from singer_sdk import typing as th
from singer_sdk.target_base import Target

from target_bexio.sinks import BexioSink


class TargetBexio(Target):
    """Sample target for Bexio."""

    name = "target-bexio"
    config_jsonschema = th.PropertiesList(
        th.Property("client_id", th.StringType, required=True),
        th.Property("client_secret", th.StringType, required=True),
        th.Property("refresh_token", th.StringType, required=True),
    ).to_dict()
    default_sink_class = BexioSink


if __name__ == "__main__":
    TargetBexio.cli()
